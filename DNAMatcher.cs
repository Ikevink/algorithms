using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DNAMatcher {
    class DNAMatcher {
        public string one, two;
        
        public DNAMatcher(string stringOne, string stringTwo) {
            one = stringOne;
            two = stringTwo;
        }

        public string Solve() {
            string longestMatch = "";
            for (int i = 0; i < one.Length; i++) {
                for (int j = 0; j < two.Length; j++) {
                    int k = 0;
                    while (k < one.Length - i && k < two.Length - j && one[i + k] == two[j + k]) {
                        if (longestMatch.Length < one.Substring(i, k + 1).Length) {
                            longestMatch = one.Substring(i, k + 1);
                        }
                        k++;
                    }
                }
            }
            return longestMatch;
        }
    }

    class Program {
        static void Main(string[] args) {
            int numCases = int.Parse(Console.ReadLine());
            string[] results = new string[numCases];

            for (int caseNum = 0; caseNum < numCases; caseNum++) {
                string wordOne = Console.ReadLine();
                string wordTwo = Console.ReadLine();
                DNAMatcher matcher = new DNAMatcher(wordOne, wordTwo);
                results[caseNum] = matcher.Solve();
            }

            for (int i = 0; i < numCases; i++) {
                Console.WriteLine("Test " + (i + 1) + ": " + results[i].Length + "-" + results[i]);
            }
        }
    }
}