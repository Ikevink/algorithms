using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Taxi {
    class TaxiPQ {
        SortedList<double, Taxi> list;

        public TaxiPQ() {
            list = new SortedList<double,Taxi>();
        }

        public void Enqueue(int k, Taxi t) {
            double key = k;
            double uniquifier = 0;
            while (list.ContainsKey(key + uniquifier)) {
                uniquifier += 0.001;
            }

            list.Add(key + uniquifier, t);
        }

        public bool Contains(Taxi t) {
            return list.ContainsValue(t);
        }

        public int Count() {
            return list.Count;
        }

        public Taxi Dequeue() {
            Taxi t = list.First().Value;
            list.Remove(list.First().Key);
            return t;
        }

        public void Replace(int key, Taxi t) {
            if (list.ContainsValue(t)) {
                list.RemoveAt(list.IndexOfValue(t));
            }
            Enqueue(key, t);
        }

        public void RemoveNeighbours(Taxi originalTaxi, int neighbourIndex) {
            for (int i = 0; i < list.Count; i++) {
                if (!list.Values[i].Equals(originalTaxi)) {
                    list.Values[i].neighbours.Remove(neighbourIndex);
                    if (list.Values[i].neighbours.Count == 0) {
                        list.RemoveAt(i);
                    }
                }
            }
        }
    }

    class Taxi {
        public int x, y;
        public List<int> neighbours;
        public int current = 0;

        public int GetMagnitude(int number) {
            if (number < 0) {
                return number * -1;
            } else {
                return number;
            }
        }

        public Taxi(string data, Person[] people, double maxStreets) {
            string[] splitString = data.Split(' ');
            x = int.Parse(splitString[0]);
            y = int.Parse(splitString[1]);

            neighbours = new List<int>();

            for (int p = 0; p < people.Length; p++) {
                if (GetMagnitude(x - people[p].x) + GetMagnitude(y - people[p].y) <= maxStreets) {
                    neighbours.Add(p);
                }
            }
        }

        static int[] stringToIntArray(string theString, char delimiter) {
            string[] splitString = theString.Split(' ');

            int[] intArray = new int[splitString.Length];

            for (int i = 0; i < intArray.Length; i++) {
                intArray[i] = int.Parse(splitString[i]);
            }
            return intArray;
        }

        static int GetTaxiResult(Taxi[] taxis, Person[] people) {
            List<int> used = new List<int>();
            TaxiPQ queue = new TaxiPQ();
            for (int n = 0; n < taxis.Length; n++) {
                if (taxis[n].neighbours.Count > 0) {
                    queue.Enqueue(taxis[n].neighbours.Count, taxis[n]);
                }
            }

            while (queue.Count() > 0) {
                Taxi node = queue.Dequeue();

                used.Add(node.neighbours[node.current]);
                queue.RemoveNeighbours(node, node.current);

            }
            return used.Count;      // default of 0s
        }

        static void Main(string[] args) {
            int numCases = int.Parse(Console.ReadLine());
            int[] results = new int[numCases];

            for (int caseNum = 0; caseNum < numCases; caseNum++) {
                int[] data = stringToIntArray(Console.ReadLine(), ' ');
                int numPeople = data[0];
                int numTaxis = data[1];
                int taxiSpeed = data[2];
                int timeLimit = data[3];

                double streetsPerSecond = (double)taxiSpeed / (double)200;  // intersection to intersection is 200 meters
                double maxStreets = (double)streetsPerSecond * (double)timeLimit;

                Person[] people = new Person[numPeople];
                for (int n = 0; n < numPeople; n++) {
                    people[n] = new Person(Console.ReadLine());
                }

                Taxi[] taxis = new Taxi[numTaxis];
                for (int n = 0; n < numTaxis; n++) {
                    taxis[n] = new Taxi(Console.ReadLine(), people, maxStreets);
                }
                results[caseNum] = GetTaxiResult(taxis, people);
            }
            
            for (int i = 0; i < numCases; i++) {
                Console.WriteLine((i + 1) + ": " + results[i]);
            }
        }
    }

    struct Person {
        public int x, y;
        public Person(int p1, int p2) {
            x = p1;
            y = p2;
        }

        public Person(string data) {
            string[] splitString = data.Split(' ');
            x = int.Parse(splitString[0]);
            y = int.Parse(splitString[1]);
        }
    }

    
}